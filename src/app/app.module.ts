import {Injector, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {SharedModule} from "@shared/shared.module";
import {AppComponent} from "@appRoot/app.component";
import {AppRoutingModule} from "@appRoot/app-routing.module";
import {CoreModule} from "@core/core.module";
import {setAppInjector} from "@shared/injectors/app.injector";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    setAppInjector(injector);
  }
}
