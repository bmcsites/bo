import {ErrorHandler, NgModule} from '@angular/core';
import {AppGlobalErrorHandler} from "@core/services/notifications/smart-log/app-global-error-handler";

@NgModule({
  providers: [
    { provide: ErrorHandler, useClass: AppGlobalErrorHandler }
    // { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
  ]
})
export class CoreModule { }
