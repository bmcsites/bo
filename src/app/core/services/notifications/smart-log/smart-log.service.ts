import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { environment } from '@env/environment';
import {LogLevel, LogLevelLabel} from "@core/services/notifications/smart-log/smart-log.enum";
import {LogEntry} from "@core/services/notifications/smart-log/log-entry";
import SafeStringify from 'fast-safe-stringify';

@Injectable({ providedIn: 'root' })

export class SmartLogService {
  level: LogLevel;
  logWithDate: boolean;
  logList: any[];
  myConsole = console;
  maxLogRecords = 2000;
  minLogRecords = 1000;

  constructor(private route: ActivatedRoute) {
    this.level = environment.production ? LogLevel.Warn : LogLevel.Debug;
    this.logWithDate = true;
    this.logList = [];

    this.route.queryParams.pipe(filter(params => params.hasOwnProperty('log'))).subscribe(params => {
      const logLevel = Number(params['log']);
      if (!isNaN(logLevel)) this.level = logLevel;
    });
  }

  fatal(message: any, ...params: any[]) {
    this.writeToLog(LogLevel.Fatal, message, ...params);
  }

  error(message: any, ...params: any[]) {
    this.writeToLog(LogLevel.Error, message, ...params);
  }

  warn(message: any, ...params: any[]){
    this.writeToLog(LogLevel.Warn, message, ...params);
  }

  info(message: any, ...params: any[]){
    this.writeToLog(LogLevel.Info, message, ...params);
  }

  debug(message: any, ...params: any[]) {
    this.writeToLog(LogLevel.Debug, message, ...params);
  }

  bugReport(userReport: any) {
    this.addToLogList(userReport);
    this.sendToServer(this.logList);
  }

  shouldLogToConsole(level: LogLevel): boolean {
    return (level >= this.level && level !== LogLevel.Off) || this.level === LogLevel.All;
  }

  writeToLog(level: LogLevel, message: any, ...params: any) {
    const entry: LogEntry = new LogEntry();
    entry.level = level;
    entry.message = message;
    entry.logWithDate = this.logWithDate;
    let error: any;
    const extraInfo = [];

    for (const [i, param] of params.entries()) {
      if (param instanceof Error) {
        if (!error) error = param;
        entry.eventName = param.name;
        entry.eventMessage = param.message;
        entry.stack = param.stack;
      } else {
        extraInfo.push(param);
      }
    }
    entry.extraInfo = extraInfo;
    const msg = entry.message || entry.eventMessage;
    if (!entry.stack) entry.stack = (new Error()).stack;

    this.thresholdLogManagement(entry);
    if (this.shouldLogToConsole(entry.level)) {
      // debug level prints object and doesnt need to be converted to string
      if (entry.level < LogLevel.Info) {
        console.log(msg);
      } else {
        this.printFormattedLog(entry, error);
      }
    }
    for (const [i, info] of extraInfo.entries()) {
      if (typeof(info) === 'object') extraInfo[i] = 'OBJECT';
    }
  }

  printFormattedLog(logEntry: LogEntry, error: Error) {
    const COLORS: any = {
      error: '\x1b[31m',   // red
      warn: '\x1b[35m',    // magenta
      info: '\x1b[34m',    // blue
      default: '\x1b[37m'  // white
    };

    const consolePrintLevel = this.getConsolePrintLevel(logEntry.level);
    const consolePrintColor = COLORS[consolePrintLevel];
    const print = (text: string) => console.log(consolePrintColor + text);

    console.group(consolePrintColor + 'LOG EVENT MESSAGE');
    const messages = [`Log Level: ${LogLevelLabel[logEntry.level]}`];
    messages.push(`Message: '${logEntry.message}'`);
    if (logEntry.eventName) messages.push(`Event Name: ${logEntry.eventName}`);
    if (logEntry.eventMessage) messages.push(`Event Message: '${logEntry.eventMessage}'`);
    print(messages.join(', '));

    if (error) console.error(error);
    if (logEntry.extraInfo.length > 0) {
      console.group(consolePrintColor + 'Extra Info:');
      logEntry.extraInfo.forEach(info => typeof(info) === 'object' ? print(SafeStringify(info, undefined, 0)) : print(info));
      console.groupEnd();
    }
    console.warn(consolePrintColor + 'Stack Trace');
    print('=======================================================================================================');
    console.groupEnd();
  }

  thresholdLogManagement(entry: any) {
    const lastLog = this.logList.length === 0 ? null : this.logList[this.logList.length - 1];
    if (lastLog && entry.message === lastLog.message && entry.level === lastLog.level && entry.extraInfo.length === lastLog.extraInfo.length) {
      lastLog.repeatCount++;
    } else {
      this.addToLogList(entry);
    }
  }

  getConsolePrintLevel(logLevel: LogLevel) {
    switch (logLevel) {
      case LogLevel.Debug:
      case LogLevel.Info:
        return 'info';
      case LogLevel.Warn:
        return 'warn';
      case LogLevel.Error:
      case LogLevel.Fatal:
        return 'error';
      default:
        return 'log';
    }
  }


  addToLogList(msg: any) {
    this.logList.push(msg);
    // to avoid memory consumption if logList is above maxLogRecords , trim it down to minLogRecords
    if (this.logList.length > this.maxLogRecords) this.logList.splice(0, this.minLogRecords);
  }

  sendToServer(obj: any) {
    // send only the last minLogRecords to the server
    if (this.logList.length >= this.minLogRecords) this.logList.splice(0, this.logList.length - this.minLogRecords);
    this.myConsole.log('Sent To Server', obj);
  }

  clearLog() {
    this.logList = [];
  }
}
