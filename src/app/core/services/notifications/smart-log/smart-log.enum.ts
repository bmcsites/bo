export enum LogLevel {
  All = 0, // all logs.
  Debug = 1, // documentation.
  Info = 2, // changes and objects.
  Warn = 3, // things that the user should know but doesnt break the code like "empty table" or "dialog refused".
  Error = 4, // errors that break or can break the code.
  Fatal = 5, // errors that break the app .
  Off = 6 // no logs .
}

export const LogLevelLabel = {
  [LogLevel.All]: 'All',
  [LogLevel.Debug]: 'Debug',
  [LogLevel.Info]: 'Info',
  [LogLevel.Warn]: 'Warn',
  [LogLevel.Error]: 'Error',
  [LogLevel.Fatal]: 'Fatal',
  [LogLevel.Off]: 'Off'
};

