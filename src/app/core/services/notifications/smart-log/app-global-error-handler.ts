import { ErrorHandler, Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import {SmartLogService} from "@core/services/notifications/smart-log/smart-log.service";
import {appInjector} from "@shared/injectors/app.injector";

@Injectable({
  providedIn: 'root'
})

export class AppGlobalErrorHandler implements ErrorHandler {
  handleError(error: Error | HttpErrorResponse): void {
    const smartLogService = appInjector.get(SmartLogService);
    smartLogService.error('an unhandled error was caught by app global error handler', error);
  }
}
