import {LogLevel} from "@core/services/notifications/smart-log/smart-log.enum";

export class LogEntry {
  entryDate: Date = new Date();
  message = '';
  eventName = '';
  eventMessage = '';
  level!: LogLevel;
  extraInfo: any[] = [];
  logWithDate = true;
  stack: any = '';
  repeatCount = 0;
}
