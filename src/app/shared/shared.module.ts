import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SafePipe} from "@shared/pipes/safe.pipe";

const imports = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule
];

const declarations = [
  SafePipe
];

@NgModule({
  declarations: declarations,
  imports: [
    imports
  ],
  exports: [imports, declarations],
  providers: []
})
export class SharedModule { }
