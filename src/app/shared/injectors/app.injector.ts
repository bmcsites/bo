import { Subject } from 'rxjs';

export let appInjector: any;
export const appInjector$ = new Subject();

export const setAppInjector = (injector: any) => {
  appInjector = injector;
  appInjector$.next(injector);
};
